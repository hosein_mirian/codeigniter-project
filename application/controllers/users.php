<?php


class Users extends CI_Controller {
	public function register() {
		$this->form_validation->set_rules('first_name', 'First Name', 'trim|required|min_length[3]');
		$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required|min_length[3]');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|min_length[3]');
		$this->form_validation->set_rules('user_name', 'User Name', 'trim|required|min_length[3]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[3]');
		$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|min_length[3]|matches[password]');

		if ($this->form_validation->run() == false) {
			$data = [
				'errors' => validation_errors(),
			];
//			$this->session->set_flashdata($data);
			$data['main_view'] = "users/register_view";
			$this->load->view('layouts/main', $data);
		} else {
			$isUserExisted = $this->user_model->check_user();
			if ($isUserExisted) {
				$this->session->set_flashdata('user_existed', 'user has already been registered!!!');
				$data['main_view'] = "users/register_view";
				$this->load->view('layouts/main', $data);
				return; // break the operation. do not execute the rest code of user creation.
			}
			if ($this->user_model->create_user()) {
				$this->session->set_flashdata('user_register', 'user has been registered');
				redirect('home');
			} else {
				$this->session->set_flashdata('user_register', 'There was a problem while registering !!!');
			}
		}
	}

	public function login() {
		$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[3]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[3]');
		$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|min_length[3]|matches[password]');

		if ($this->form_validation->run() == false) {
			$data = [
				'errors' => validation_errors(),
			];
			$this->session->set_flashdata($data);
			redirect('home');
		} // if the form validation is done. now its time to call the model function to check the user availability
		else {
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$user_id = $this->user_model->login_user($username, $password);
			if ($user_id) {
				$user_data = [
					'user_id' => $user_id,
					'username' => $username,
					'logged_in' => true,
				];
				$this->session->set_userdata($user_data); // this is a stable session and needs to be destroy otherwise will stay
				$this->session->set_flashdata('login_success', "You are now logged in"); // this dynamically can be seen in the view and also no need to remove the session
        redirect('home');
//				$data['main_view'] = "admin_view";
//				$this->load->view('layouts/main', $data);
			} else {
				$this->session->set_flashdata('login_failed', "Sorry Your are not logged in"); // this dynamically can be seen in the view and also no need to remove the session
				redirect('home');
			}
		}
	}

	public function logout() {
		$this->session->sess_destroy();
		redirect('home');
	}


	/*    public function show()
      {
          // $this->load->model('user_model'); because we used autoload php and configured it automatically to load the initial config for us
          $result = $this->user_model->get_users(2);
          // $data['myVar']='content of my var'; // we defined a variable name myVar to access it in the view side of the app

          $data['results'] = $result;

          $this->load->view('users_view', $data);
          // foreach ($result as $object) wrong way. we need to pass it to our users_view to show the data
          // {
          //     echo('id :'.$object->id .'  username :'.$object->username . ' password:'.$object->password).'<br>';
          // }
      }

      public function insert()
      {
          $user_name = 'peter';
          $user_password = 'secret';
          $this->user_model->create_user(
              [
                  'username' => $user_name,
                  'password' => $user_password,
              ]
          );
      }

      public function update() {
      $id = 3;
      $username = "William";
      $password = "not so secret";

      $this->user_model->update_user([
        'username' => $username,
        'password' => $password
        ],$id);
    }

      public function delete() {
          $id = 3;
          $this->user_model->delete_user($id);
    }*/

}

