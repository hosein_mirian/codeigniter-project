<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>Page Title</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<div style="text-align:center">
	<h1>Home_View is Me</h1>

	<p class="bg-success">
		<?php if ($this->session->flashdata('login_success')): ?>
			<?php echo $this->session->flashdata('login_success') ?>
		<?php endif; ?>
	</p>
	<p class="bg-danger">
		<?php if ($this->session->flashdata('login_failed')): ?>
			<?php echo $this->session->flashdata('login_failed') ?>
		<?php endif; ?>
	</p>

	<p class="bg-success">
		<?php if ($this->session->flashdata('user_register')): ?>
			<?php echo $this->session->flashdata('user_register') ?>
		<?php endif; ?>
	</p>


</div>
</body>
</html>
