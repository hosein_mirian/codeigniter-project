<!DOCTYPE html>
<html>
<head>
	<title>Users_View</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
	<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
</head>

<body style="margin-top: 40px">

<nav class="navbar navbar-expand-md bg-dark navbar-dark fixed-top">
	<a class="navbar-brand" href="#">CRM MIRIAN</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="collapsibleNavbar">
		<ul class="navbar-nav">
			<li class="nav-item">
				<a class="nav-link" href="<?php echo base_url('home') ?>">Home</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="<?php echo base_url('users/register') ?>">Register</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="#">Project</a>
			</li>
		</ul>
	</div>
</nav>
<br>


<div class="container-fluid">

	<div class="row">
		<div class="col-sm-3">
			<?php $this->load->view('users/login_view'); ?>
		</div>
		<div class="col-sm-9">
			<?php $this->load->view($main_view); ?>
		</div>
	</div>

</div>
</body>
</html>
