<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8"/>
	<title>login Title</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<?php if ($this->session->userdata('logged_in')): ?>
	<?php echo 'you are logged in as ' . $this->session->userdata('username'); ?>

	<form action="<?php echo base_url('users/logout'); ?>" method="post">
		<button type="submit" class="btn btn-primary">Logout</button>
	</form>

<?php else: ?>

	<div class="container">
		<h2>Login Form</h2>
		<?php if ($this->session->flashdata('errors')): ?>
			<?php echo $this->session->flashdata('errors') ?>
		<?php endif; ?>

		<form action="<?php echo base_url('users/login'); ?>" method="post">
			<div class="form-group">
				<label for="username">User Name:</label>
				<input type="text" class="form-control" id="username" placeholder="Enter user name" name="username">
			</div>
			<div class="form-group">
				<label for="password">Password:</label>
				<input type="password" class="form-control" id="password" placeholder="Enter password" name="password">
			</div>
			<div class="form-group">
				<label for="confirm_password">confirm Password:</label>
				<input type="password" class="form-control" id="confirm_password" placeholder="Confirm password"
							 name="confirm_password">
			</div>
			<button type="submit" class="btn btn-primary">Submit</button>
		</form>
	</div>
<?php endif; ?>

</body>
</html>
