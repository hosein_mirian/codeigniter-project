<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8"/>
	<title>Register Title</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<div class="container">
	<h2>Register Form</h2>
	<p class="bg-danger" style="color:white">
		<?php if ($this->session->flashdata('user_existed')): ?>
			<?php echo $this->session->flashdata('user_existed') ?>
		<?php endif; ?>
	</p>

	<?php echo validation_errors('<p class="bg-danger" style="color:white;">'); ?>

	<form action="<?php echo base_url('users/register'); ?>" method="post">
		<div class="form-group">
			<label for="first_name">First Name:</label>
			<input type="text" class="form-control" id="first_name" placeholder="Enter First Name" name="first_name">
		</div>
		<div class="form-group">
			<label for="last_name">Last Name:</label>
			<input type="text" class="form-control" id="last_name" placeholder="Enter Last Name" name="last_name">
		</div>
		<div class="form-group">
			<label for="email">Email:</label>
			<input type="email" class="form-control" id="email" placeholder="Enter Email"
						 name="email">
		</div>
		<div class="form-group">
			<label for="user_name">User Name:</label>
			<input type="text" class="form-control" id="user_name" placeholder="Enter User Name"
						 name="user_name">
		</div>
		<div class="form-group">
			<label for="password">Password:</label>
			<input type="password" class="form-control" id="password" placeholder="Enter Password"
						 name="password">
		</div>
		<div class="form-group">
			<label for="confirm_password">Confirm Password:</label>
			<input type="password" class="form-control" id="confirm_password" placeholder="Confirm Password"
						 name="confirm_password">
		</div>
		<button type="submit" class="btn btn-primary">Register</button>
	</form>
</div>
</body>
</html>
