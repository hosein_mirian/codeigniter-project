<?php


class User_Model extends CI_Model {
	/*    public function get_users($user_id)
      {
          // $query = $this->db->get('users');
          // return $query->result();
          // $query = $this->db->query('select * from users');
          // //return $query->num_rows(); // returns total rows number
          // return $query->num_fields(); // return fields number
          $this->db->where('id', $user_id);
          $query = $this->db->get('users');
          return $query->result();
      }

      public function create_user($data)
      {
          $this->db->insert('users', $data);
      }


      public function update_user($data, $id)
      {
          $this->db->where(['id' => $id]);
          $this->db->update('users', $data);
      }

      public function delete_user($id)
      {
          $this->db->where(['id' => $id]);
          $this->db->delete('users');
      }*/
	public function create_user() {
		$option = ['cost' => 12];
		$encrypted_pass = password_hash($this->input->post('password'), PASSWORD_BCRYPT, $option);
		$data = [
			'first_name' => $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name'),
			'email' => $this->input->post('email'),
			'username' => $this->input->post('user_name'),
			'password' => $encrypted_pass,
		];
		$insert_data = $this->db->insert('users', $data);
		return $insert_data;
	}

	public function check_user() {
		$email = $this->input->post('email');
		$this->db->where('email', $email);
		$result = $this->db->get('users');
		if ($result->num_rows() == 1) {
			return true; // which means there is one other user registered with this email earlier and break the operation.
		} else {
			return false;
		}
	}

	public function login_user($username, $password) {
		$this->db->where('username', $username);
		$result = $this->db->get('users');
		$db_password = $result->row(6)->password; // 6 because our column number 6 is password.
		if (password_verify($password, $db_password)) {
			return $result->row(0)->id;
		} else {
			return false;
		}
	}


}
